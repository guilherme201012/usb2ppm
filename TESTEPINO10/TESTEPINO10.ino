/*
  Software para receber dados de PWM via serial do MATLAB e encadear conforme protocolo PPM por RX
  a RC  (radio controle) HObbynco Spektrum 18 canais

  Version 0.0.0

  Guilherme Soares e Silva
*/
#define numeroCanais 2
//Timer Initialization
#include "TimerOne.h"
#define PPM_framelength 22000    //Maximum framelength in counter ticks
#define PPMPausePulse 400            //Pause between pluses in counter ticks
#define PPM_max 2000.0
#define PPM_min 1000.0
#define PPM_range (PPM_max - PPM_min)
// Pin Definitions
#define led_PIN  7 //LED Status LED
#define armsw_PIN 12 // Arm switch pin
#define mode_PIN 13 //Trim enable switch
#define ppmoutPIN 10 // PPM output

//Mode ID
#define mode_trim 0
#define mode_serial 1
#define onState 1
int mode = 0;



//String Variable initialization
boolean stringComplete = false;  // whether the string is complete
char cmd_Char[26] = "";
String inputString[2];         // a string to hold incoming data
int cmd_val[2];
int pulsePPM[2];
int trim_val[4];
int inChar = 0;
int aux;


//Timer variables
int timer_accumulator = 0;         //accumulator. Used to calculate the frame padding
int timer_ptr = 0;                 //timer array pointer
int pulses[numeroCanais];
int number_of_outputs = 8;
unsigned long timeStartChanel;
unsigned long timeFinalChanel;

int count = 650;
int arm_stat = 0;

void setup() {
  //Set Pinmodes
  pinMode(ppmoutPIN, OUTPUT);
  digitalWrite(ppmoutPIN, onState);
  pinMode(armsw_PIN, INPUT);
  pinMode(led_PIN, OUTPUT);
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);
  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH);
  digitalWrite(led_PIN, HIGH);
  Serial.begin(115200); // Initialize Serial
  pulses[1] = 1020;
  pulses[2] = 1000;
  //  init_buffer(); //Initialize buffer
////
//    Timer1.initialize(PPM_framelength); // comprimento do sinal ppm
//    Timer1.attachInterrupt(timerIsr); // function that generate the ppm signal
}
// Main Loop will run at 50Hz
void loop() {
//  digitalWrite(ppmoutPIN, !onState);
//  delayMicroseconds(PPMPausePulse);
//  digitalWrite(ppmoutPIN, onState);
//  delayMicroseconds(PPMPausePulse);
  for (int n = 0; n < numeroCanais; n++) {
      digitalWrite(ppmoutPIN, !onState);
      timeStartChanel = micros();
      delayMicroseconds(PPMPausePulse);
      digitalWrite(ppmoutPIN, onState);
      timeFinalChanel = micros();
      delayMicroseconds((int) (pulses[n] - (timeFinalChanel - timeStartChanel)));
    }
//digitalWrite(ppmoutPIN, !onState);
//      timeStartChanel = micros();
//      delayMicroseconds(PPMPausePulse);
//      digitalWrite(ppmoutPIN, onState);
//      timeFinalChanel = micros();
//      delayMicroseconds((int) (pulses[2] - (timeFinalChanel - timeStartChanel)));
//  digitalWrite(2, HIGH);
//if (stringComplete == true)
//delay(2000);
//digitalWrite(2, HIGH);
// digitalWrite(4, HIGH);
// digitalWrite(led_PIN, HIGH);
}
//digitalWrite(2, HIGH);
  //  delay(22) ;
  // -x-x-x-x-x-x-x-x-xDEBUG
//    digitalWrite(4, HIGH);
//    digitalWrite(led_PIN, LOW);
//    delay(1000);
//    digitalWrite(led_PIN, HIGH);
//    delay(1000);
//    stringComplete = false;
//    serial_Event();
//     -x-x-x-x-x-x-x-x-x
//  if (stringComplete==true){
//      digitalWrite(4, LOW);
//    delay(22);
//    serial_monitor();
//    digitalWrite(4, LOW);
//  digitalWrite(led_PIN, HIGH);
//  digitalWrite(2, LOW);
//  }
  //   sw_read(); // Read mode switch / arm switches
  //   serial_Event(); //Read String to buffer

  //   if(mode==mode_trim){ //Enter Trim Mode
  //    sw_readTrim();
  //    }
  //  else {// Serial Mode
  //    if (stringComplete) {//Execute if serial is received
  //    serial_Decode(); //Decode and copy the packet
  ////    ppm_command(mode);
  //    }
  //  }

  //Set pulse values for PPM signal
  //  sw_led();
  //  timer_loopcount(); //Counter for handshake

  //Always disable serial monitor before operating, only for development use
  //serial_monitor(); //For debugging through Serial Monitor
  //serial_handshake();

  //  delay(20);




