/*
  Software para receber dados de PWM via serial do MATLAB e encadear conforme protocolo PPM por RX
  a RC  (radio controle) HObbynco Spektrum 18 canais

  Version 0.0.0

  Guilherme Soares e Silva
*/
//Timer Initialization
#include "TimerOne.h"
#define PPM_framelength 22000    //Maximum framelength in counter ticks
#define PPMPausePulse 400            //Pause between pluses in counter ticks
// Pin Definitions
#define led_PIN  7 //LED Status LED
#define armsw_PIN 12 // Arm switch pin
#define mode_PIN 13 //Trim enable switch
#define ppmoutPIN 10 // PPM output

//Mode ID
#define mode_trim 0
#define mode_serial 1
#define onState 1
int mode = 0;



//String Variable initialization
boolean stringComplete = false;  // whether the string is complete
String inputString;         // a string to hold incoming data
int cmd_val[2];
int pulsePPM[2][100];
int inChar = 0;
//Timer variables
int numeroCanais = 8;
int pulses[8];
int p = 0;
int i = 0;
int k = 0;
int j = 0;

void setup() {
  //Set Pinmodes
  pinMode(ppmoutPIN, OUTPUT);
  digitalWrite(ppmoutPIN, onState);
  pinMode(armsw_PIN, INPUT);
  pinMode(led_PIN, OUTPUT);
  pinMode(2, OUTPUT);
  digitalWrite(led_PIN, LOW);
  Serial.begin(115200); // Initialize Serial
  // Setup Timer
  Timer1.initialize(PPM_framelength); // comprimento do sinal ppm
  Timer1.attachInterrupt(timerIsr); // function that generate the ppm signal


}
// Main Loop will run at 50Hz
void loop() {
    if (stringComplete == false){
      serial_Event();
    }

}


