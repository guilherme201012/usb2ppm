#include <TimerOne.h>
// define as variaveis que serão utilizadas globalmente
const int pinoA0 = A0;
const int pinoA1 = A1;
unsigned int valorSensor = 0;
int IN1 = 52 ;
int IN2 = 53 ;
int velocidadeMotor = 3;
int velocidadeRotacaoMotor = 5;
int cont=0;
int auxs=0;
int valorFiltrado=0;

String inputString = "";         // a String to hold incoming data
boolean stringComplete = false;  // whether the string is complete
  
void setup() {
  //define os estados dos pinos e a comunicação  
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);     
  pinMode(velocidadeMotor, OUTPUT);
  Serial.begin(9600); //Inicia a serial com Baud Rate de 9600 
  inputString.reserve(200);
  //define o tempo no qual a interrupção será executada
  Timer1.initialize(1e4); // set a timer, microsenconds
  Timer1.attachInterrupt( timerIsr ); // attach the service routine here

}

void loop() {
  if (stringComplete) { //caso receba um valor novo de PWM
    velocidadeRotacaoMotor = inputString.toInt();
    inputString = "";
    stringComplete = false;
    if (velocidadeRotacaoMotor<0)
    {
      //Serial.println(velocidadeRotacaoMotor);
      velocidadeRotacaoMotor=-velocidadeRotacaoMotor;
      digitalWrite(IN2,LOW);
      digitalWrite(IN1,HIGH);
    }
    else
    {
      digitalWrite(IN2,HIGH);
      digitalWrite(IN1,LOW);
    }
  }
  analogWrite(velocidadeMotor,velocidadeRotacaoMotor);
}

void timerIsr() //funcao chamada pelo timer
{
    valorSensor = analogRead(pinoA1); //le a porta analogica
    auxs=auxs+valorSensor;
    cont=cont+1;
    if(cont>=5)
    {
      valorFiltrado=(int)(auxs/5);
      auxs=0;
      cont=0;
      Serial.println(valorFiltrado); //envia o sinal para a serial
    }
}

void serialEvent() { //funcão executada quando o arduino recebe algo na porta serial
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    if (inChar == '#') {
      stringComplete = true;
    }
    else
    {      
      inputString += inChar;
    }
  }    
  //Serial.println(inputString);
}
