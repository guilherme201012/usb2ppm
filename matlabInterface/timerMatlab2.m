function timerEx
% inputs
t = 1:1:25;
% time-dependent functions
sin_t = sin(t);
cos_t = cos(t);
% loop for time-dependent measurements
n = numel(t);
figure, xlim([min(t) max(t)]), ylim([-2 2]);
hold on
%Plot first point, store handles so we can update the data later.
h(1) = plot (t(1), sin_t(1));
h(2) = plot (t(1), cos_t(1));
%Build timer
T = timer('Period',0.001,... %period
          'ExecutionMode','fixedRate',... %{singleShot,fixedRate,fixedSpacing,fixedDelay}
          'BusyMode','drop',... %{drop, error, queue}
          'TasksToExecute',n-1,...          
          'StartDelay',0,...
          'TimerFcn',@tcb,...
          'StartFcn',[],...
          'StopFcn',[],...
          'ErrorFcn',[]);
% Start it      
start(T);
      %Nested function!  Has access to variables in above workspace
      function tcb(src,evt)
%           tic
          %What task are we on?  Use this instead of for-loop variable ii
          taskEx = get(src,'TasksExecuted');
          %Update the x and y data.
          set(h(1),'XData',t(1:taskEx),'YData',sin_t(1:taskEx));
          set(h(2),'XData',t(1:taskEx),'YData',cos_t(1:taskEx));
          drawnow; %force event queue flush
          taskEx
%           tempo = toc;
      end
  end