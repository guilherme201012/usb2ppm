function criaEntrada2
PPMRange = 2000-1000;
PPMMin = 1000;
ref = 9;
tempo = 0:0.001:.022*ref;
Y = 0.75.*sin(2*pi*2*tempo);
PWM = geraPWM(Y, tempo);
tempoAcum = 0;
indicePulsoInic = 0;
aux = [];tempoDiscreto = [];
while (tempoAcum < tempo(end))
    indicePulsoFim = find(tempo<=.022+tempoAcum);
    tempoAcum = tempoAcum+.022;
    tempoDiscreto = [tempoDiscreto;tempoAcum];
    indice = find(PWM(indicePulsoInic+1:indicePulsoFim(end)-1)==1);
    if isempty (indice)
        pulse = .022;
    else
        pulse = tempo(indice(end)+indicePulsoInic)-tempoAcum+.022;
    end
    indicePulsoInic = indicePulsoFim(end);
    aux = [aux;pulse];
end
pulse = int16(aux/.022*PPMRange+PPMMin);
entrada = [tempoDiscreto pulse pulse];
save('ENTRADA.mat', 'entrada');
end