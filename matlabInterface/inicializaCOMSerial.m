function portaCOM = inicializaCOMSerial(nomePortaCOM)
%inicializa a comunicação com a porta serial entre Arduio e Matlab. Ele
%garante que esta ocorrendo a comunicação entre arduino e Matlab e o Matlab
%pode começar a receber/transmitir dados. Deve ser usada antes de qualquer
%programa que a interface entre os dois sistemas exista.
%Referência: http://www.matlabarduino.org/
%ENTRADA: endereço da porta COM
%SAIDA: 1 quando a comunicação esta completa e 0 caso contrário

portaCOM = serial(nomePortaCOM);
set(portaCOM,'DataBits', 8);
set(portaCOM,'StopBits', 1);
set(portaCOM,'BaudRate', 115200);
set(portaCOM,'Parity', 'none');
portaCOM.BytesAvailableFcnMode = 'terminator';
portaCOM.OutputBufferSize = 15000;
% portaCOM.BytesAvailableFcn = @intcon1;
% portaCOM.OutputEmptyFcn = @instrcallback;
fopen(portaCOM);
% leituraMatlab = fread(portaCOM,1,'uchar');
% while (leituraMatlab ~= 'a')
%     leituraMatlab = fread(portaCOM,1,'uchar');
% end
% if (leituraMatlab == 'a')
%     disp('lendo porta serial');
% end
% fprintf(portaCOM,'%c','m');
disp('comunicação serial estabelecida.');
% fscanf(portaCOM,'%c');