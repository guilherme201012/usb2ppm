function [PWM,pwmVetor,baseTempo] = geraPWM(sinal, t,PPMFrameLenght, periodoReferencia,batenteServo)
% F1= 2/PPMFrameLenght;
% A=max(sinal);
c=batenteServo*sawtooth(2*pi*1/periodoReferencia*t);%Carrier sawtooth
%% to DEBUG
% subplot(3,1,1);
% plot(t,c);
% xlabel('time');
% ylabel('Amplitude');
% title('Carrier sawtooth wave');
% grid on;
%%
m = sinal;%Message amplitude must be less than Sawtooth
%% to DEBUG
% subplot(3,1,2);
% plot(t,m);
% xlabel('Time');
% ylabel('Amplitude');
% title('Message Signal');
% grid on;
%%
n=length(c);%Length of carrier sawtooth is stored to �n�
k=1;tReferencia = 0;baseTempo = [];
for i=1:n%Comparing Message and Sawtooth amplitudes
if (m(i)>=c(i))
    PWM(i)=1;
    if (t(i)+0.0010 < PPMFrameLenght + tReferencia)% problemas de arredondamento (POSSIVELMENTE) est�o criando PWMs que n�o existem
        pwmVetor(k) = t(i)-tReferencia;
    end
    if mod(t(i), PPMFrameLenght)==0 & t(i)>0
        k=k+1;
        baseTempo = [baseTempo;t(i)-tReferencia];
        tReferencia = t(i);
    end
else
    PWM(i)=0;
        if mod(t(i), PPMFrameLenght)==0
            k=k+1;
            baseTempo = [baseTempo;t(i)-tReferencia];
            tReferencia = t(i);
        end
end
end
% toDEBUG
% subplot(3,1,3);
% plot(t,PWM);
% xlabel('Time');
% ylabel('Amplitude');
% title('plot of PWM');
% ylim([0 1.5]);
%%
end
