function fechaCOMSerial(portaCOM)
%fecha a comunica��o com a porta serial entre Arduio e Matlab. 
%Refer�ncia: http://www.matlabarduino.org/
%ENTRADA: endere�o da porta COM
%SAIDA: n�o aplic�vel
fclose(portaCOM);
delete(portaCOM);
disp('Porta Serial Fechada');
end