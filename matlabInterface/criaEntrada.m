function entrada = criaEntrada
PPMRange = 2000-1000;
PPMMin = 1000;
PPMFrameLenght = 0.022;%segundos
tempoEntrada = 9;%segundos
batenteServo = 9;% deve ser maior que o m�ximo valor atribuido ao sinal
periodoReferencia = PPMFrameLenght;
passo = periodoReferencia/220;
tempo = 0:passo:tempoEntrada;
Amplitude = 3;
% f = [1:.5:60];
% fi = linspace(1,60,max(size(tempo)));
fi=50;
% Y = Amplitude*sin(2*pi*fi.*tempo);
Y = [Amplitude*ones(max(size(tempo),1))];
[PWM,pwmVetor,baseTempo] = geraPWM(Y, tempo, PPMFrameLenght,periodoReferencia, batenteServo);
% tempoAcum = 0;
% indicePulsoInic = 0;
% aux = [];tempoDiscreto = [];tempoAux = 0;
% while (tempoAcum < tempo(end))
%     indicePulsoFim = find(tempo<=periodoReferencia+tempoAcum);
%     tempoAcum = tempoAcum+PPMFrameLenght;
%     tempoAux = tempoAux + PPMFrameLenght;
%     tempoDiscreto = [tempoDiscreto; tempoAux];
%     indice = find(PWM(indicePulsoInic+1:indicePulsoFim(end)-1)==1);
%     if isempty (indice)
%         pulse = PPMFrameLenght;
%     else
% %         pulse = tempo(indice(end)+indicePulsoInic)-tempoAcum+PPMFrameLenght;
%         pulse = tempo(max(indice));
%     end
% %     hold on;subplot(3,1,3);plot(pulse+tempoAcum-PPMFrameLenght,1,'ro');
%     indicePulsoInic = indicePulsoFim(end);
%     aux = [aux; pulse];
% end
pulse = int16(pwmVetor/PPMFrameLenght*PPMRange+PPMMin);
entrada = [pulse' pulse'];
save('ENTRADA.mat', 'entrada','baseTempo');
end