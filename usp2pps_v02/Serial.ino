/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
 
void serialEvent() {
  int j=0;
  digitalWrite(led_PIN, HIGH);
  Serial.print(2);
  init_buffer(); //Reiniitialize buffer
  while (Serial.available()) {
    inChar = (char)Serial.read();  // get the new byte:
    if(inChar == 'L'){
      j=0;
    }
    else if(inChar =='M'){
      j=1;
    }
    else if (inChar == '#') {
      stringComplete = true;     // if the incoming character is a newline, set a flag
      break;
    }
    else {
      inputString[j] += inChar; // add it to the inputString:
    }
  }
}

void serial_Decode(){
 for(int j=0; j<1; j++){
  cmd_val[j] =  inputString[j].toInt();
 }
}

void init_buffer(){
  for(int j=0; j<1; j++){
    inputString[j]= ""; //Initialize string buffer
    cmd_val[j]=1000; //Initialize decoded command values
   }
   stringComplete = false; //Disable flag
}

void serial_monitor(){
  /*Serial.print(" armSW: "); Serial.print(arm_stat);
  Serial.print(" modeSW: "); Serial.println(mode);
  
  Serial.print("Ch1: "); Serial.print(pulses[0]);
  Serial.print(" Ch2: "); Serial.print(pulses[1]);
  Serial.print(" Ch3: "); Serial.print(pulses[2]);
  Serial.print(" Ch4: "); Serial.print(pulses[3]);
  Serial.print(" Ch5: "); Serial.println(pulses[4]);
  
  Serial.print("trim1: ");  Serial.print(trim_val[0]);
  Serial.print(" trim2: "); Serial.print(trim_val[1]);
  Serial.print(" trim3: "); Serial.print(trim_val[2]);
  Serial.print(" trim4: "); Serial.print(trim_val[3]);
  */
}

void serial_handshake(){
  if(count>50){
  Serial.println("Ack");
  }
}

