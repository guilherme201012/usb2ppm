/*
  USB2PPM Firmware
  This program receives commands for 4 channels from the PC via string and convert it into 9 channels of ppm signals to interface with the transmitter.
  Currently this code has been tested only on Turnigy 9X. Other transmitters may be compatible but may have volatage issues

  Version 1.0.0
  
  Jaeyoung Lim
*/
#define chanel_number 8
//Timer Initialization
#include "TimerOne.h"
#define PPM_framelength 22000    //Maximum framelength in counter ticks
#define PPM_pausePulse 400            //Pause between pluses in counter ticks
#define PPM_max 2000.0
#define PPM_min 1000.0
#define PPM_range (PPM_max - PPM_min)
// Pin Definitions
#define led_PIN  7 //LED Status LED
#define armsw_PIN 12 // Arm switch pin
#define mode_PIN 13 //Trim enable switch
#define ppmout_PIN 10 // PPM output

//Mode ID
#define mode_trim 0
#define mode_serial 1
#define onState 1
int mode=0;



//String Variable initialization
boolean stringComplete = false;  // whether the string is complete
char cmd_Char[26]="";
String inputString[2];         // a string to hold incoming data
int cmd_val[4];
int trim_val[4];
int inChar=0;
int aux;


//Timer variables
int timer_accumulator = 0;         //accumulator. Used to calculate the frame padding
int timer_ptr = 0;                 //timer array pointer
int pulses[chanel_number];
int number_of_outputs =8;
unsigned long timeStartChanel;
unsigned long timeFinalChanel;

int count=650;
int arm_stat =0;

void setup() {
  //Set Pinmodes
  pinMode(ppmout_PIN, OUTPUT);
  digitalWrite(ppmout_PIN,onState);
  pinMode(armsw_PIN, INPUT);
  pinMode(led_PIN, OUTPUT);

  Serial.begin(115200); // Initialize Serial
  init_buffer(); //Initialize buffer
  
  Timer1.initialize(PPM_framelength); // comprimento do sinal ppm
  Timer1.attachInterrupt(timerIsr); // function that generate the ppm signal  
}
// Main Loop will run at 50Hz
void loop() {
//   sw_read(); // Read mode switch / arm switches
//   serial_Event(); //Read String to buffer
  
//   if(mode==mode_trim){ //Enter Trim Mode
//    sw_readTrim();
//    }
//  else {// Serial Mode
//    if (stringComplete) {//Execute if serial is received
//    serial_Decode(); //Decode and copy the packet
////    ppm_command(mode);
//    }
//  }
  
   //Set pulse values for PPM signal
//  sw_led();
//  timer_loopcount(); //Counter for handshake
  
  //Always disable serial monitor before operating, only for development use
  //serial_monitor(); //For debugging through Serial Monitor
  //serial_handshake();
  
  delay(20);
}

