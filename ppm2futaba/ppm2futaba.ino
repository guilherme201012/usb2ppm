#define chanel_number 6
#define PPM_max 2000.0
#define PPM_min 1000.0
#define PPM_range (PPM_max - PPM_min)
#define PPM_FrLen 20000
#define PPM_PulseLen 300
#define onState 1
#define sigPin 10
#define DEBUG 0

float ppm[chanel_number];
float pot[chanel_number];
unsigned long time_start;
unsigned long time_final;
unsigned long time_start_chanel;
unsigned long time_final_chanel;

void setup(){
  time_start = 0;
  
  for(int i=0; i<chanel_number; i++){
    ppm[i] = PPM_min;
    pot[i] = 0;
  }

  pinMode(sigPin, OUTPUT);
  digitalWrite(sigPin,onState);
  
  if(DEBUG){
    Serial.begin(9600);
  }
}

void loop(){
  if(DEBUG){
    Serial.print("Time frame: ");
    Serial.println((micros()-time_start));
  }
  time_start = micros();
  for(int i=0; i<chanel_number; i++){
    digitalWrite(sigPin,!onState);
    time_start_chanel = micros();
    switch(i){
      case 1:
        pot[i] = analogRead(A1);
        break;
      case 2:
        pot[i] = analogRead(A2);
        break;
      case 3:
        pot[i] = analogRead(A3);
        break;
      case 4:
        pot[i] = analogRead(A4);
        break;
      case 5:
        pot[i] = analogRead(A5);
        break;
      case 6:
        pot[i] = analogRead(A6);
        break;
      case 7:
        pot[i] = analogRead(A7);
        break;
      default:
        pot[i] = analogRead(A0);
        break;
    }        
    ppm[i] = ((pot[i]+1)/1024.0*PPM_range+PPM_min);
    delayMicroseconds(PPM_PulseLen);
    if(DEBUG){
      Serial.print(i);
      Serial.print(": ");
      Serial.print(pot[i]);
      Serial.print(" -- ");
      Serial.print(ppm[i]);
      Serial.print(" | ");
    }
    digitalWrite(sigPin,onState);
    time_final_chanel = micros();
    delayMicroseconds((int) (ppm[i]-(time_final_chanel-time_start_chanel)));
  }
  time_final = micros();
  if(DEBUG){
    Serial.print("Time elapsed: ");
    Serial.println((time_final-time_start));
  }
  delayMicroseconds(PPM_FrLen-(time_final-time_start));
}
