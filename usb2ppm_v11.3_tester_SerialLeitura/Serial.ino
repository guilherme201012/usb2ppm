/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/

void serial_Event() {
  while (Serial.available() > 0) {
    inChar = (char)Serial.read();  // get the new byte:
    if (inChar == 'L') {
      j = 0;
      p = p+1;
    }
    else if (inChar == 'M') {
      j = 1;
      k = k+1;
    }
    else if (inChar == '#') {
       digitalWrite(4, HIGH);
       stringComplete = true;     // if the incoming character is a newline, set a flag
       break;
    }
    else if (inChar == '!') {
      if (j == 0){
        pulsePPM[j][p] =  inputString.toInt();
      }
      else if (j == 1){
        pulsePPM[j][k] =  inputString.toInt();
      }
      inputString = ""; //Inicializa string buffer
    }
    else {
      inputString += (char)inChar; // add it to the inputString:
    }
  }
}

void init_buffer(int m) {
  for (int q = 0; q < numeroCanais; q++) {
         pulsePPM[q][m] = 1000; //Initialize decoded command values 
         Serial.println(pulsePPM[q][m]);
  }
}
