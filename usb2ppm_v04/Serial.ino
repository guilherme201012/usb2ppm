/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/

void serialEvent() {
  int j = 0;
  //  int k = 0;
  init_buffer(); //Reiniitialize buffer
  while (Serial.available()) {
    inChar = (char)Serial.read();  // get the new byte:
    if (inChar == 'L') {
      j = 0;
      //      k=0;
    }
    else if (inChar == 'M') {
      j = 1;
      //      k = 0;
    }
    else if (inChar == '#') {
      for (int j = 0; j < 1; j++) {
        pulsePPM[j] =  inputString[j].toInt();
//        pulsePPM[j] = pulsePPM[j]*10;
      }
      stringComplete = true;     // if the incoming character is a newline, set a flag
      digitalWrite(led_PIN, LOW);
      digitalWrite(2, HIGH);
    }
    else {
      inputString[j] += inChar; // add it to the inputString:
      //          digitalWrite(4, HIGH);
      //  digitalWrite(led_PIN, LOW);
    }
    //    digitalWrite(4, LOW);
    //    delay(500);
    //    digitalWrite(4, HIGH);
    //    delay(500);
  }
}

void serial_Decode() {
  //  int m = 10;
  //  for (int k = 0; k < m; k++) {
  //    for (int j = 0; j < 1; j++) {
  //      cmd_val[j] =  inputString[j].toInt();
  //    }
  //  }
}

void init_buffer() {
  for (int j = 0; j < numeroCanais; j++) {
    inputString[j] = ""; //Initialize string buffer
    pulsePPM[j] = 1000; //Initialize decoded command values
  }
//      stringComplete = false; //Disable flag
//  digitalWrite(2, LOW);
//  digitalWrite(4, HIGH);
}

void serial_monitor() {
  //  digitalWrite(4, HIGH);
  //  delay(22);
  //  Serial.print(cmd_val[1][1]);
  //  Serial.print(cmd_val[2][1]);
  //  for (int j = 0; j < 700; j++) {
  //    digitalWrite(4, LOW);
  //    delay(20);
  //    digitalWrite(4, HIGH);
  //    delay(22);
  //    digitalWrite(led_PIN, LOW);
  //    delay(20);
  //    digitalWrite(led_PIN, HIGH);
  //    delay(22);
  //  }
  //  stringComplete == false;
  /*Serial.print(" armSW: "); Serial.print(arm_stat);
    Serial.print(" modeSW: "); Serial.println(mode);

    Serial.print("Ch1: "); Serial.print(pulses[0]);
    Serial.print(" Ch2: "); Serial.print(pulses[1]);
    Serial.print(" Ch3: "); Serial.print(pulses[2]);
    Serial.print(" Ch4: "); Serial.print(pulses[3]);
    Serial.print(" Ch5: "); Serial.println(pulses[4]);

    Serial.print("trim1: ");  Serial.print(trim_val[0]);
    Serial.print(" trim2: "); Serial.print(trim_val[1]);
    Serial.print(" trim3: "); Serial.print(trim_val[2]);
    Serial.print(" trim4: "); Serial.print(trim_val[3]);
  */
}
//
void handShake() {
  Serial.print('a');
  //  char a = 'b';
  //  while (a !='a'){
  //    a = Serial.read();
  //  }
  //  digitalWrite(4, LOW);
}
