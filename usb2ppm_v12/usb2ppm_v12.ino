/*
  Software para receber dados de PWM via serial do MATLAB e encadear conforme protocolo PPM por RX
  a RC  (radio controle) HObbynco Spektrum 18 canais

  Version 0.0.0

  Guilherme Soares e Silva
*/
//Timer Initialization
#include "TimerOne.h"
#define PPM_framelength 22000    //Maximum framelength in counter ticks
#define PPMPausePulse 400            //Pause between pluses in counter ticks
// Pin Definitions
#define ppmoutPIN 10 // PPM output

//Mode ID
#define onState 1

//String Variable initialization
boolean stringComplete = false;  // whether the string is complete
boolean zerarBuffer = false;
String inputString;         // a string to hold incoming data
int pulsePPM[2][448];
int inChar = 0;
//Timer variables
int numeroCanais = 8;
int pulses[8];
int p = 0;
int i = 0;
int k = 0;
int j = 0;
int l = 0;
int n = 0;
int q = 0;
int m = 0;
int r = 0;
int s = 0;
int u = 0;

void setup() {
  //Set Pinmodes
  pinMode(ppmoutPIN, OUTPUT);
  pinMode(4, OUTPUT);
  digitalWrite(ppmoutPIN, onState);
  Serial.begin(115200); // Initialize Serial
  // Setup Timer
  Timer1.initialize(PPM_framelength); // comprimento do sinal ppm
  Timer1.attachInterrupt(timerIsr); // function that generate the ppm signal
}
// Main Loop will run at 50Hz
void loop() {
    if (!stringComplete && !zerarBuffer){
      serial_Event();
//      for(l=1;l<=400;l++){
//        pulsePPM[0][l] = 2000;
//        pulsePPM[1][l] = 1000; 
//      } 
//      if(l >= 400){
//        digitalWrite(4, HIGH);
//        stringComplete = true;
//      }   
    }
    if (zerarBuffer){
      for(u=1;u<=400;u++){
        digitalWrite(4, LOW);
        pulsePPM[0][u] = 0;
        pulsePPM[1][u] = 0; 
        if (u >= 400){
          digitalWrite(4, LOW);
          zerarBuffer = false; 
      }
      }
    }
}

